<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_ayrah');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'jJU..^;,*kp9}a-Eb!Gn<y+l6 5Zj_kTJdfzpUc9j_Ncw;8EvILj_-5(^1I@aFSg');
define('SECURE_AUTH_KEY',  'Lo1~ZtWza.Bb$#_@mv$_9|Y`,k|iJ10|{7sQoS=}48tot2&!~bJH]>nuLQ1p}/fD');
define('LOGGED_IN_KEY',    ':J%Oxj>F$~)l#amXWJKiNLCWzLht.pYC,+p%3}y8!>z&quYd%[bRU@dIX]cx_&[9');
define('NONCE_KEY',        '?Ps0p2*bZnqM0L$N_=HOq6Fo1oW7S9$4)iDI`v4*r@K$A$}I[qT+y%#hVj?MZdL*');
define('AUTH_SALT',        '2#`$ h7Ihu3gh{Q(+VZ+)K:]t{/#h}1<O!#v~8q/FOT@}CR-%C$%]~xS!lAi=p$E');
define('SECURE_AUTH_SALT', 'pI >Cz0SbF #<j>*[StPkNeVZmj(~U=xxd2B(qn Tof6CsUjH3Fe&CN PNpEQ-] ');
define('LOGGED_IN_SALT',   ';//Q;$>/O]@YB(QWN:I~B?h4RjDa6!;6^.4bEMK^$Y}J 2I{Vj2au#mXjtDdC:b(');
define('NONCE_SALT',       '/3m$xG,Y|X.;EryNx=z]7*)7fxqDs&EXa-=0Kv`@(]-Z-(H7yMq4$8IZo/r;^>()');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
